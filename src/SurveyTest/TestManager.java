package SurveyTest;

import IO.Input;
import IO.Output;
import Menu.QuestionMenu;

public class TestManager extends SurveyManager {

    public TestManager(Input i, Output o) {
        super(i, o);
        dir = "./Survey/";
        ext = ".survey";
    }

    public String[] getExisting() {
        return Test.get_existing();
    }

    public void create() {
        String name = i.getStringInput("Please enter a name for the test: ");
        Survey s = new Test(i, o, name);
        new QuestionMenu(i, o, s).doChoice();
    }

//    public Survey load(String name) {
//        return Test.load(name, i, o);
//    }
}
