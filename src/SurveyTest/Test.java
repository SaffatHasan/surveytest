package SurveyTest;

import IO.Input;
import IO.Output;
import Menu.CARMenu;
import Question.Question;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class Test extends Survey implements Serializable {
    protected static String dir = "./Test/";
    protected static String ext = ".test";

    public Test(Input i, Output o, String name) {
        super(i, o, name);
    }

    /**
     * Gets a list of all surveys
     *
     * @return the names of all surveys in ./<dir>/<survey_name><ext>
     */
    public static String[] get_existing() {
        // Make sure the save directory exists (or create it)
        create_save_dir(dir);

        // Get the directory
        File directory = new File(dir);

        // Get all the files
        File[] files = directory.listFiles();

        // Get all valid files into a vector
        Vector<String> survey_names = new Vector<>();
        for (int i = 0; i < files.length; i++) {
            if (files[i].getName().endsWith(ext)) {
                survey_names.add(files[i].getName().replace(ext, ""));
            }
        }

        // Return the
        return survey_names.toArray(new String[0]);
    }

    public void grade() {
        o.enumerated_display(users);
        int choice = i.getIntegerInput("Please select a user to grade");
        while (choice < 1 || choice > users.size()) {
            choice = i.getIntegerInput();
        }

        // To get computer indexing
        choice = choice - 1;

        List<Boolean> grades = new ArrayList<>();
        for (Question q : questions) {
            // -1 to switch from human to computer indexing
            grades.add(q.grade(choice));
        }

        int correct = 0;
        int total = 0;

        for (boolean b : grades) {
            if (b) {
                correct++;
            }
            total++;
        }
        o.display(users.get(choice) + " got " + correct + " correct out of " + total);
    }

    /**
     * Same as super class except it also asks if the user wants to modify/add CAR
     */
    public void modify() {
        for (Question q : questions) {
            q.display();
            q.modify();
            if (q.gradeable) {
                // modify existing
                q.modify_CAR();

                // add CAR
                new CARMenu(i, o, q).doChoice();
            }
        }
        save();
    }

    /** File IO functions below **/

    /**
     * Adds CAR to a question before adding it to the test
     *
     * @param q
     */
    public void addQuestion(Question q) {
        if (q.gradeable) {
            // Always add at least 1 CAR
            new CARMenu(i, o, q).doChoice();
        }
        questions.add(q);
        save();
    }

    public void save() {
        create_save_dir(dir);
        try {
            FileOutputStream file_out = new FileOutputStream(dir + name + ext);
            ObjectOutputStream object_out = new ObjectOutputStream(file_out);
            object_out.writeObject(this);
        } catch (IOException e) {
            o.display("Failed to save survey '" + name + "'");
            o.display(e.toString());
        }
    }
}
