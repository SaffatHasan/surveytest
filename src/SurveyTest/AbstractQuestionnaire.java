package SurveyTest;

import IO.Input;
import IO.Output;

import java.io.*;

public abstract class AbstractQuestionnaire implements Serializable {
    protected String dir;
    protected String ext;

    public AbstractQuestionnaire(String dir, String ext) {
        this.dir = dir;
        this.ext = ext;
    }

    protected static void create_save_dir(String dir) {
        File directory = new File(dir);
        if (!directory.exists()) {
            directory.mkdir();
        }
    }

    public AbstractQuestionnaire load(String name, Input i, Output o) {
        try {
            File f = new File(dir + name + ext);
            FileInputStream fi = new FileInputStream(f);
            ObjectInputStream oi = new ObjectInputStream(fi);
            Survey s = (Survey) oi.readObject();
            s.set_io(i, o);
            return s;
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(1);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public abstract void display();
    public abstract void tabulate();
    public abstract void take();
    public abstract void modify();


}
