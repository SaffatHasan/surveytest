package SurveyTest;

import IO.Input;
import IO.Output;
import Menu.QuestionMenu;

public class SurveyManager {
    protected Input i;
    protected Output o;
    protected String dir;
    protected String ext;

    public SurveyManager(Input i, Output o) {
        this.i = i;
        this.o = o;
        dir = "./Survey/";
        ext = ".survey";
    }

    public String[] getExisting() {
        return Survey.get_existing();
    }

    public void create() {
        String name = i.getStringInput("Please enter a name for the survey: ");
        Survey s = new Survey(i, o, name);
        new QuestionMenu(i, o, s).doChoice();
    }

    public AbstractQuestionnaire load(String name) {
        return new Survey().load(name, i, o);
    }
}
