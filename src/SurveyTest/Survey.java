package SurveyTest;

import IO.Input;
import IO.Output;
import Question.Question;

import java.io.*;
import java.util.Vector;

/**
 * Survey class
 */
public class Survey extends AbstractQuestionnaire {
    public static String dir = "./Survey/";
    public static String ext = ".survey";
    protected Input i;
    protected Output o;
    protected String name;
    protected Vector<Question> questions;
    protected Vector<String> users;

    public Survey() {
        super("./Survey/", ".survey");
    }

    public Survey(Input i, Output o, String name) {
        super("./Survey/", ".survey");
        this.i = i;
        this.o = o;
        this.name = name;
        this.questions = new Vector<>();
        this.users = new Vector<>();
    }


    /**
     * Gets a list of all surveys
     *
     * @return the names of all surveys in ./<dir>/<survey_name><ext>
     */
    public static String[] get_existing() {
        // Make sure the save directory exists (or create it)
        create_save_dir(dir);

        // Get the directory
        File directory = new File(dir);

        // Get all the files
        File[] files = directory.listFiles();

        // Get all valid files into a vector
        Vector<String> survey_names = new Vector<>();
        for (int i = 0; i < files.length; i++) {
            if (files[i].getName().endsWith(ext)) {
                survey_names.add(files[i].getName().replace(ext, ""));
            }
        }

        // Return the
        return survey_names.toArray(new String[0]);
    }

    /**
     * Goes through each question and asks if you want to modify it
     */
    public void modify() {
        for (Question q : questions) {
            q.display();
            q.modify();
        }
        save();
    }

    /**
     * Takes a Survey by displaying each question and then gathering user input
     * Only saves if the survey is completed
     */
    public void take() {
        users.add(i.getStringInput("Please enter your name"));
        for (Question q : questions) {
            q.display();
            q.addUserResponse();
        }
        save();
    }

    /**
     * Adds a question and then saves the survey
     *
     * @param q
     */
    public void addQuestion(Question q) {
        questions.add(q);
        save();
    }

    /**
     * displays the classes
     */
    public void display() {
        if (questions.size() == 0) {
            o.display("There are no questions!");
            return;
        }
        o.display_questions(questions);
    }

    /**
     * displays the current class along with user responses
     */
    public void tabulate() {
        if (questions.size() == 0) {
            o.display("There are no questions!");
            return;
        }
        for (Question q : questions) {
            q.display();
            q.displayResponses();
        }
    }

    /**
     * Saves the current class to a file
     */
    public void save() {
        create_save_dir(dir);
        try {
            FileOutputStream file_out = new FileOutputStream(dir + name + ext);
            ObjectOutputStream object_out = new ObjectOutputStream(file_out);
            object_out.writeObject(this);
        } catch (IOException e) {
            o.display("Failed to save survey '" + name + "'");
            o.display(e.toString());
        }
    }

    /**
     * Sets the IO objects for this class and its container classes. Primarily used in load because
     * IO cannot be serialized.
     *
     * @param i
     * @param o
     */
    protected void set_io(Input i, Output o) {
        this.i = i;
        this.o = o;
        for (Question q : questions) {
            q.setIO(i, o);
        }
    }
}
