package Menu;

import IO.Input;
import IO.Output;
import SurveyTest.AbstractQuestionnaire;
import SurveyTest.SurveyManager;

public class SurveyMenu extends Menu {
    protected SurveyManager sm;

    public SurveyMenu(Input i, Output o) {
        super(i, o);
        String c[] = {
                "Create a new Survey",
                "Display a Survey",
                "Take a Survey",
                "Tabulate a Survey",
                "Modify a Survey",
                "Quit"
        };
        initializeChoices(c);
        sm = new SurveyManager(i, o);
    }

    public boolean executeChoice(int userChoice) {
        AbstractQuestionnaire survey;
        switch (userChoice) {
            case 1:
                sm.create();
                return true;
            case 2:
                // display
                survey = new SelectMenu(i, o, sm).getForm();
                if (survey == null) {
                    return true;
                }
                survey.display();
                return true;
            case 3:
                survey = new SelectMenu(i, o, sm).getForm();
                if (survey == null) {
                    return true;
                }
                survey.take();
                return true;
            case 4:
                survey = new SelectMenu(i, o, sm).getForm();
                if (survey == null) {
                    return true;
                }
                survey.tabulate();
                return true;
            case 5:
                survey = new SelectMenu(i, o, sm).getForm();
                if (survey == null) {
                    return true;
                }
                survey.modify();
            case 6:
                // quit this menu
                return false;
            default:
                return true;
        }
    }
}
