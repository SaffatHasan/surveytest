package Menu;

import IO.Input;
import IO.Output;
import Question.Question;

public class CARMenu extends Menu {
    Question q;

    public CARMenu(Input i, Output o, Question q) {
        super(i, o);
        this.q = q;
        String[] c = {
                "Add Correct Answer Response",
                "Continue"
        };
        initializeChoices(c);
    }

    @Override
    public boolean executeChoice(int userChoice) {
        q.display();
        switch (userChoice) {
            case 1:
                q.addCAR();
                return true;
            case 2:
                return false;
            default:
                return true;
        }
    }
}
