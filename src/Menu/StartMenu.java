package Menu;

import IO.Input;
import IO.Output;

public class StartMenu extends Menu {

    public StartMenu(Input i, Output o) {
        super(i, o);
        String c[] = {"Survey", "Test", "Quit"};
        initializeChoices(c);
    }

    public boolean executeChoice(int userChoice) {
        switch (userChoice) {
            case 1:
                Menu sm = new SurveyMenu(i, o);
                sm.doChoice();
                return true;
            case 2:
                Menu tm = new TestMenu(i, o);
                tm.doChoice();
                return true;
            case 3:
                return false;
            default:
                return true;
        }
    }
}
