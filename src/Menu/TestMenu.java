package Menu;

import IO.Input;
import IO.Output;
import SurveyTest.AbstractQuestionnaire;
import SurveyTest.Survey;
import SurveyTest.Test;
import SurveyTest.TestManager;

public class TestMenu extends SurveyMenu {
    public TestMenu(Input i, Output o) {
        super(i, o);
        String c[] = {
                "Create a new Test",
                "Display a Test",
                "Take a Test",
                "Tabulate a Test",
                "Modify a Test",
                "Grade a test",
                "Quit"
        };
        initializeChoices(c);
        sm = new TestManager(i, o);
    }

    public boolean executeChoice(int userChoice) {
        AbstractQuestionnaire survey;
        switch (userChoice) {
            case 1:
                sm.create();
                return true;
            case 2:
                // display
                survey = new SelectMenu(i, o, sm).getForm();
                if (survey == null) {
                    return true;
                }
                survey.display();
                return true;
            case 3:
                survey = new SelectMenu(i, o, sm).getForm();
                if (survey == null) {
                    return true;
                }
                survey.take();
                return true;
            case 4:
                survey = new SelectMenu(i, o, sm).getForm();
                if (survey == null) {
                    return true;
                }
                survey.tabulate();
                return true;
            case 5:
                survey = new SelectMenu(i, o, sm).getForm();
                if (survey == null) {
                    return true;
                }
                survey.modify();
                return true;
            case 6:
                survey = new SelectMenu(i, o, sm).getForm();
                ((Test) survey).grade();
                return true;
            case 7:
                // quit this men6
                return false;
            default:
                return true;
        }
    }
}