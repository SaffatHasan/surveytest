package Menu;

import IO.Input;
import IO.Output;
import Question.*;
import SurveyTest.Survey;

public class QuestionMenu extends Menu {
    Survey s;

    public QuestionMenu(Input i, Output o, Survey s) {
        super(i, o);
        this.s = s;
        String c[] = {
                "Add a new T/F question",
                "Add a new multiple choice question",
                "Add a new short answer question",
                "Add a new Essay question",
                "Add a new ranking question",
                "Add a new matching question",
                "Quit"
        };
        initializeChoices(c);
    }

    @Override
    public boolean executeChoice(int userChoice) {
        switch (userChoice) {
            case 1:
                // TF
                s.addQuestion(new TFQuestion(i, o));
                return true;
            case 2:
                // MC question
                s.addQuestion(new MultipleChoiceQuestion(i, o));
                return true;
            case 3:
                // short answer
                s.addQuestion(new ShortAnswerQuestion(i, o));
                return true;
            case 4:
                // Essay
                s.addQuestion(new EssayQuestion(i, o));
                return true;
            case 5:
                // ranking
                s.addQuestion(new RankingQuestion(i, o));
                return true;
            case 6:
                // matching
                s.addQuestion(new MatchingQuestion(i, o));
                return true;
            case 7:
                // quit
                return false;
            default:
                return true;
        }
    }

}
