package Menu;

import IO.Input;
import IO.Output;

import java.util.Vector;

public abstract class Menu {
    /**
     * Represents any menu that might be displayed to the user
     **/
    protected Vector<String> choices;
    protected Output o;
    protected Input i;

    /**
     * Base constructor that fills in input and output objects
     *
     * @param i
     * @param o
     */
    public Menu(Input i, Output o) {
        this.o = o;
        this.i = i;
    }

    /**
     * Displays choices to the screen
     */
    public void displayChoices() {
        o.enumerated_display(choices);
    }

    /**
     * Fetches a valid user choice
     *
     * @return
     */
    public int getChoice() {
        int userChoice;
        do {
            userChoice = i.getIntegerInput();
        } while (userChoice <= 0 || userChoice > choices.size());
        return userChoice;
    }

    /**
     * Instantiates choices and adds each item in c to it
     **/
    public void initializeChoices(String[] c) {
        choices = new Vector<>();
        for (int i = 0; i < c.length; i++) {
            choices.add(c[i]);
        }
    }

    /**
     * Calls execute choice and does some rudimentary user input checking
     */
    public void doChoice() {
        if (choices.size() == 0) {
            o.display("No choices to display.");
            return;
        }
        while (true) {
            displayChoices();
            boolean do_continue = executeChoice(getChoice());
            if (!do_continue) {
                break;
            }
        }
    }

    /**
     * Required abstract method
     *
     * @param userChoice
     * @return
     */
    public abstract boolean executeChoice(int userChoice);
}
