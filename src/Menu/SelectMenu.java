package Menu;

import IO.Input;
import IO.Output;
import SurveyTest.AbstractQuestionnaire;
import SurveyTest.Survey;
import SurveyTest.SurveyManager;

public class SelectMenu extends Menu {
    SurveyManager sm;

    public SelectMenu(Input i, Output o, SurveyManager sm) {
        super(i, o);
        this.sm = sm;
        String c[] = sm.getExisting();
        initializeChoices(c);
    }

    @Override
    public boolean executeChoice(int userChoice) {
        String survey_name = choices.elementAt(userChoice - 1);
        AbstractQuestionnaire survey = sm.load(survey_name);
        survey.display();
        return false;
    }

    public AbstractQuestionnaire getForm() {
        if (choices.size() == 0) {
            o.display("There are no options to display.");
            return null;
        }
        displayChoices();
        int userChoice = getChoice();
        String survey_name = choices.elementAt(userChoice - 1);
        return sm.load(survey_name);
    }
}
