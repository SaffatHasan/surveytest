package IO;

import Question.Question;

import java.io.Serializable;
import java.util.Vector;

public abstract class Output implements Serializable {
    public abstract void display(String s);

    public abstract void enumerated_display(Vector<String> choices);

    public abstract void itemized_display(Vector<String> choices);

    public abstract void itemized_display(Vector<String> choices, boolean primary);

    public abstract void display_questions(Vector<Question> questions);

    public abstract char[] get_itemization_array();

    public abstract char[] get_itemization_array_2();
}
