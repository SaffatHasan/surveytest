package IO;

import java.io.Serializable;
import java.util.Vector;

public abstract class Input implements Serializable {
    public abstract String getStringInput();

    public abstract String getStringInput(String prompt);

    public abstract char getCharInput();

    public abstract char getCharInput(String prompt);

    public abstract Integer getIntegerInput();

    public abstract Integer getIntegerInput(String prompt);

    public abstract boolean get_user_confirmation(String prompt);

    public abstract int get_user_choice(Vector<String> c);
}
