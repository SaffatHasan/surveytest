package IO;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Vector;

public class ConsoleInput extends Input {
    // Transient as the scanner cannot be saved to a file
    // must be instantiated at runtime
    protected transient Scanner s;
    protected Output o;

    public ConsoleInput(Output o) {
        s = new Scanner(System.in);
        this.o = o;
    }

    /**
     * Gets user input until a non-empty line is retrieved
     *
     * @return A non-empty string of user input
     */
    public String getStringInput() {
        return getStringInput("Please enter your response: ");
    }

    public String getStringInput(String prompt) {
        if (prompt.length() > 0) {
            o.display(prompt);
        }
        String p = s.nextLine();
        if (p == null || p.equals("")) {
            return getStringInput("");
        }
        return p;
    }

    /**
     * Used for single character choices such as multiple choice
     *
     * @return Character representing a choice
     */
    public char getCharInput() {
        return getCharInput("Please enter your character response:");
    }

    public char getCharInput(String prompt) {
        return getStringInput(prompt).charAt(0);
    }

    /**
     * Collects user input. Recursive call if the user enters a non-integer type
     *
     * @return integer representing a user input
     */
    public Integer getIntegerInput(String prompt) {
        o.display(prompt);
        try {
            return s.nextInt();
        } catch (InputMismatchException e) {
            s.next();
            return getIntegerInput();
        }
    }

    public Integer getIntegerInput() {
        return getIntegerInput("Please enter your integer response:");
    }

    public boolean get_user_confirmation(String prompt) {
        System.out.println(prompt + " ('y' for yes)");
        return getCharInput("") == 'y';
    }

    @Override
    public int get_user_choice(Vector<String> c) {
        o.display("Please select an option");
        o.itemized_display(c);
        String options = new String(o.get_itemization_array());
        char resp = getCharInput();
        int resp_idx = options.indexOf(resp);

        while (resp_idx < 0 || resp_idx > c.size()) {
            resp = getCharInput();
            resp_idx = options.indexOf(resp);
        }

        return resp_idx;
    }

}
