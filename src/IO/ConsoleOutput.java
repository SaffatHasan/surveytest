package IO;

import Question.Question;

import java.util.Vector;

public class ConsoleOutput extends Output {
    public void display(String s) {
        System.out.println(s);
    }

    public void enumerated_display(Vector<String> s) {
        for (int i = 0; i < s.size(); i++) {
            System.out.printf("%d: %s\n", i + 1, s.elementAt(i));
        }
    }

    /**
     * Displays vector items on one line with A), B), ...
     */
    public void itemized_display(Vector<String> choices, boolean primary) {
        char[] itemization;
        if (primary) {
            itemization = get_itemization_array();
        } else {
            itemization = get_itemization_array_2();
        }
        for (int i = 0; i < choices.size(); i++) {
            System.out.printf("\t%c) %s", itemization[i], choices.elementAt(i));
        }
        System.out.println();
    }

    public void itemized_display(Vector<String> choices) {
        itemized_display(choices, true);
    }

    public char[] get_itemization_array() {
        return "abcdefghijklmnopqrstuvwxyz".toCharArray();
    }

    public char[] get_itemization_array_2() {
        return "123456789".toCharArray();
    }

    @Override
    public void display_questions(Vector<Question> questions) {
        for (int i = 0; i < questions.size(); i++) {
            System.out.printf("%d) ", i + 1);
            questions.elementAt(i).display();
            System.out.println();
        }
    }

}
