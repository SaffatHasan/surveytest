package Question;

import IO.Input;
import IO.Output;
import Response.Response;
import Response.StringResponse;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class ShortAnswerQuestion extends EssayQuestion {
    protected int max_char;

    public ShortAnswerQuestion(Input i, Output o) {
        super(i, o);
        gradeable = true;
        max_char = i.getIntegerInput("Please enter the character limit:");
    }

    public void display() {
        o.display(prompt + " (Max " + max_char + " characters)");
    }

    public Response getResponse() {
        String resp = i.getStringInput();

        while (resp.length() > max_char) {
            resp = i.getStringInput();
        }
        return new StringResponse(resp);
    }

    @Override
    public void displayResponses() {
        if (userResponse.size() == 0) {
            o.display("There are no responses");
            return;
        }
        Map<String, Integer> mp = new HashMap<>();

        for (Response r : userResponse) {
            String response = (String) r.getResponse();
            if (mp.keySet().contains(response)) {
                mp.put(response, mp.get(response) + 1);
            } else {
                mp.put(response, 1);
            }
        }

        for (String key : mp.keySet()) {
            o.display(key + " " + mp.get(key));
        }
    }

    @Override
    public void modify() {
        modify_prompt();

        if (!i.get_user_confirmation("Would you like to modify the character limit?)")) {
            return;
        }
        max_char = i.getIntegerInput("Please enter a new character limit");
    }

    public void modify_CAR() {
        if (!i.get_user_confirmation("Would you like to modify a CAR?")) {
            return;
        }
        Vector<String> choices = new Vector<>();
        for (Response r : correctAnswer) {
            choices.add((String)r.getResponse());
        }

        int resp_idx = i.get_user_choice(choices);

        String CAR = i.getStringInput("Please enter a new CAR");

        correctAnswer.set(resp_idx,new StringResponse(CAR));
    }
}
