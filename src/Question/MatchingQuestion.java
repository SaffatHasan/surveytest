package Question;

import IO.Input;
import IO.Output;
import Response.IntListResponse;
import Response.Response;

import java.util.*;

public class MatchingQuestion extends RankingQuestion {
    Vector<String> matchedChoices;

    public MatchingQuestion(Input i, Output o) {
        super(i, o);
        matchedChoices = setChoices();
    }

    public void display() {
        super.display();
        o.itemized_display(matchedChoices, false);
    }

    /**
     * Gets the associated item to a matched item
     * Does some logic checking to make sure the user pertains to a
     * one-to-one function
     *
     * @return IntListResponse representing what maps to what
     */
    public Response getResponse() {
        String responses = new String(o.get_itemization_array());
        String matched_responses = new String(o.get_itemization_array_2());

        int size = matchedChoices.size();
        List<Integer> userResp = new ArrayList<>();

        for (int idx = 0; idx < size; idx++) {
            char resp = i.getCharInput("Select your match for " + responses.charAt(idx));
            Integer resp_idx = matched_responses.indexOf(resp);
            if (resp_idx.equals(-1)) {
                o.display("Invalid choice.");
                idx--;
                continue;
            }
            boolean retry = false;
            for (int j = 0; j < idx; j++) {
                if (userResp.get(j).equals(resp_idx)) {
                    o.display("You've already selected this value.");
                    break;
                }
            }
            if (retry) {
                idx--;
                continue;
            }

            userResp.add(resp_idx);
        }

        return new IntListResponse(userResp);
    }

    /**
     * Tabulates responses by
     * 2
     * A 2
     * B 1
     * C 3
     * <p>
     * 1
     * A 1
     * B 3
     * C 2
     * etc
     */
    @Override
    public void displayResponses() {
        if (userResponse.size() == 0) {
            o.display("There are no responses");
            return;
        }
        Map<List<Integer>, Integer> mp = new HashMap<>();
        for (Response r : userResponse) {
            List<Integer> resp = (List<Integer>) r.getResponse();
            if (mp.keySet().contains(resp)) {
                mp.put(resp, mp.get(resp) + 1);
            } else {
                mp.put(resp, 1);
            }
        }

        String options = new String(o.get_itemization_array());
        String matched_opts = new String(o.get_itemization_array_2());

        for (List<Integer> key : mp.keySet()) {
            o.display(mp.get(key).toString());
            for (int idx = 0; idx < key.size(); idx++) {
                char choice = options.charAt(idx);
                char matched_choice = matched_opts.charAt(key.get(idx));
                o.display(choice + " " + matched_choice);
            }
            // newline
            o.display("");
        }
    }


    public void modify() {
        modify_prompt();
        if (i.get_user_confirmation("Would you like to modify the choices?")) {
            int resp_idx = i.get_user_choice(choices);
            String new_choice = i.getStringInput("Please enter a new one");

            choices.set(resp_idx, new_choice);
        }
        if (i.get_user_confirmation("Would you like to modify the matched choices?")) {
            int resp_idx = i.get_user_choice(matchedChoices);
            String new_choice = i.getStringInput("Please enter a new one");

            matchedChoices.set(resp_idx, new_choice);
        }
    }

    public void modify_CAR() {
        // Incomplete
    }
}
