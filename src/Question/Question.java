package Question;

import IO.Input;
import IO.Output;
import Response.Response;

import java.io.Serializable;
import java.util.Vector;

/**
 * Represents the base class for the question
 * Contains CAR for test class
 */
public abstract class Question implements Serializable {
    public Vector<Response> correctAnswer;
    public boolean gradeable;
    public String prompt;
    protected Input i;
    protected Output o;
    protected Vector<Response> userResponse;

    /**
     * Base constructor
     *
     * @param i
     * @param o
     * @param prompt
     */
    public Question(Input i, Output o, String prompt) {
        this.i = i;
        this.o = o;
        this.prompt = prompt;
        this.userResponse = new Vector<>();
        this.correctAnswer = new Vector<>();
        this.gradeable = true;
    }

    /**
     * Base constructor that does not include prompt
     *
     * @param i
     * @param o
     */
    public Question(Input i, Output o) {
        this(i, o, i.getStringInput("Please enter the prompt for this question:"));
    }

    public abstract void display();

    public void addUserResponse() {
        userResponse.add(getResponse());
    }

    public void addCAR() {
        correctAnswer.add(getResponse());
    }

    public abstract Response getResponse();

    public void setIO(Input i, Output o) {
        this.i = i;
        this.o = o;
    }

    public abstract void displayResponses();

    public boolean grade(int user) {
        if (correctAnswer.size() == 0) {
            // There are no correct answers, return false
            return false;
        }

        Response resp = userResponse.get(user);
        for (Response CAR : correctAnswer) {
            if (CAR.equals(resp)) {
                return true;
            }
        }
        return false;
    }

    /**
     * method to modify any question properties
     */
    public abstract void modify();

    /**
     * method to modify CAR
     */
    public abstract void modify_CAR();

    /**
     * modify the prompt for a question
     */
    public void modify_prompt() {
        if (i.get_user_confirmation("Would you like to modify the prompt?")) {
            prompt = i.getStringInput("Please enter a new prompt");
        }
    }
}
