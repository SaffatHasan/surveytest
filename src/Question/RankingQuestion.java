package Question;

import IO.Input;
import IO.Output;
import Response.IntListResponse;
import Response.Response;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RankingQuestion extends MultipleChoiceQuestion {

    public RankingQuestion(Input i, Output o) {
        super(i, o);
    }

    public Response getResponse() {
        int size = choices.size();
        String items = new String(o.get_itemization_array());

        List<Integer> resp = new ArrayList<>();

        for (int idx = 0; idx < size; idx++) {
            char u_resp = i.getCharInput("Please enter your choice");
            Integer resp_idx = items.indexOf(u_resp);
            if (resp_idx.equals(-1)) {
                o.display("Invalid choice");
                idx--;
                continue;
            }
            boolean retry = false;
            for (int j = 0; j < idx; j++) {
                if (resp_idx.equals(resp.get(j))) {
                    o.display("You've already selected that one");
                    retry = true;
                    break;
                }
            }
            if (retry) {
                idx--;
                continue;
            }
            resp.add(resp_idx);
        }
        return new IntListResponse(resp);
    }

    /**
     * Want to display
     * 1
     * A 1
     * B 2
     * C 3
     * <p>
     * 2
     * B 1
     * C 2
     * A 3
     */
    public void displayResponses() {
        if (userResponse.size() == 0) {
            o.display("There are no responses");
            return;
        }
        Map<List<Integer>, Integer> mp = new HashMap<>();

        // Gather all items into a hash set
        for (Response r : userResponse) {
            List<Integer> response = (List<Integer>) r.getResponse();
            if (mp.keySet().contains(response)) {
                mp.put(response, mp.get(response) + 1);
            } else {
                mp.put(response, 1);
            }
        }
        String options = new String(o.get_itemization_array());

        // Output replies, grouped with numbering
        o.display("Replies:");
        for (List<Integer> key : mp.keySet()) {
            o.display(mp.get(key).toString());
            for (int idx = 0; idx < key.size(); idx++) {
                // +1 for human indexing
                o.display(options.charAt(key.get(idx)) + ": " + (idx + 1));
            }
            // Newline
            o.display("");
        }
    }

    public void modify() {
        modify_prompt();
        if (!i.get_user_confirmation("Would you like to modify the choices?")) {
            return;
        }

        int resp_idx = i.get_user_choice(choices);
        String new_choice = i.getStringInput("Please enter a new one");
        choices.set(resp_idx, new_choice);
    }

}
