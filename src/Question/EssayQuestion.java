package Question;

import IO.Input;
import IO.Output;
import Response.Response;
import Response.StringResponse;

public class EssayQuestion extends Question {
    /**
     * Also assigns gradeable to be false
     */
    public EssayQuestion(Input i, Output o, String prompt) {
        super(i, o, prompt);
        gradeable = false;
    }

    /**
     * Constructor for Essay. Also assigns gradeable to false;
     *
     * @param i
     * @param o
     */
    public EssayQuestion(Input i, Output o) {
        super(i, o);
        gradeable = false;
    }

    public void display() {
        o.display(prompt);
    }

    public Response getResponse() {
        return new StringResponse(i.getStringInput());
    }

    /**
     * Tabulates the responses for this question
     */
    @Override
    public void displayResponses() {
        if (userResponse.size() == 0) {
            o.display("There are no responses");
            return;
        }
        for (Response r : userResponse) {
            o.display((String) r.getResponse());
        }
    }

    @Override
    public void modify() {
        modify_prompt();
    }

    public void modify_CAR() {
        // Essays do not have CAR
    }
}
