package Question;

import IO.Input;
import IO.Output;

import java.util.Vector;

public class TFQuestion extends MultipleChoiceQuestion {

    public TFQuestion(Input i, Output o) {
        super(i, o);
        setNum_choices();
        choices = setChoices();
    }

    @Override
    public void setNum_choices() {
        num_choices = 2;
    }

    public Vector<String> setChoices() {
        Vector<String> c = new Vector<>();
        c.add("True");
        c.add("False");
        return c;
    }

    public void modify() {
        modify_prompt();
    }
}
