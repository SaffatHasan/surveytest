package Question;

import IO.Input;
import IO.Output;
import Response.IntResponse;
import Response.Response;

import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class MultipleChoiceQuestion extends Question {
    protected int num_choices;
    protected Vector<String> choices;

    public MultipleChoiceQuestion(Input i, Output o) {
        super(i, o);
        setNum_choices();
        choices = setChoices();
    }

    public void setNum_choices() {
        do {
            num_choices = i.getIntegerInput("Please enter the number of choices:");
        } while (num_choices <= 0);
    }

    public Vector<String> setChoices() {
        Vector<String> c = new Vector<>();
        for (int idx = 1; idx <= num_choices; idx++) {
            c.add(i.getStringInput("Please enter choice " + idx));
        }
        return c;
    }

    public void display() {
        o.display(prompt);
        o.itemized_display(choices);
    }

    /**
     * Gets the element number that corresponds to an answer
     * i.e. a = 0, b = 1, ...
     *
     * @return IntResponse
     */
    public Response getResponse() {

        String options = new String(o.get_itemization_array());

        char user_response = i.getCharInput();
        int resp = options.indexOf(user_response);

        while (resp < 0 || resp >= num_choices) {
            user_response = i.getCharInput("Invalid choice, please enter your response");
            resp = options.indexOf(user_response);
        }
        return new IntResponse(resp);
    }

    public void displayResponses() {
        if (userResponse.size() == 0) {
            o.display("There are no responses");
            return;
        }
        Map<Integer, Integer> mp = new HashMap<>();

        for (Response r : userResponse) {
            Integer response = (Integer) r.getResponse();
            if (mp.keySet().contains(response)) {
                mp.put(response, mp.get(response) + 1);
            } else {
                mp.put(response, 1);
            }
        }
        String options = new String(o.get_itemization_array());

        for (Integer key : mp.keySet()) {
            o.display(options.charAt(key) + ": " + mp.get(key));
        }
    }


    public void modify() {
        modify_prompt();
        if (i.get_user_confirmation("Would you like to modify the choices?")) {
            int resp_idx = i.get_user_choice(choices);
            String new_choice = i.getStringInput("Please enter a new one");

            choices.set(resp_idx, new_choice);
        }
    }

    public void modify_CAR() {
        if (!i.get_user_confirmation("Would you like to modify a CAR?")) {
            return;
        }
        Vector<String> car = new Vector<>();
        String options = new String(o.get_itemization_array());
        for (Response CAR : correctAnswer) {
            Integer val = (Integer) CAR.getResponse();
            String choice = String.valueOf(options.charAt(val));
            car.add(choice);
        }
        int resp_idx = i.get_user_choice(car);

        char user_response = i.getCharInput();
        int resp = options.indexOf(user_response);

        while (resp < 0 || resp >= num_choices) {
            user_response = i.getCharInput("Invalid choice, please enter your response");
            resp = options.indexOf(user_response);
        }

        correctAnswer.set(resp_idx, new IntResponse(resp));
    }
}
