import IO.ConsoleInput;
import IO.ConsoleOutput;
import IO.Input;
import IO.Output;
import Menu.Menu;
import Menu.StartMenu;

public class Main {
    /**
     * Main driver for the program
     * Loads input output and starts the primary menu
     *
     * @param args
     */
    public static void main(String[] args) {
        Output output = new ConsoleOutput();
        Input input = new ConsoleInput(output);
        Menu CreateMenu = new StartMenu(input, output);
        CreateMenu.doChoice();
    }
}


