package Response;

import java.util.List;

public class IntListResponse extends Response {
    protected List<Integer> response;

    public IntListResponse(List<Integer> resp) {
        this.response = resp;
    }

    public List<Integer> getResponse() {
        return this.response;
    }

    @Override
    public boolean equals(Response r) {
        if (!(r instanceof IntListResponse)) {
            return false;
        }
        List<Integer> other_response = ((IntListResponse) r).getResponse();
        return other_response.equals(getResponse());
    }
}
