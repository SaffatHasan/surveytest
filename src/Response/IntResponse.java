package Response;

public class IntResponse extends Response {
    protected Integer response;

    public IntResponse(Integer resp) {
        this.response = resp;
    }

    public Integer getResponse() {
        return this.response;
    }

    public boolean equals(Response r) {
        if (!(r instanceof IntResponse)) {
            return false;
        }

        Integer other_response = (Integer) r.getResponse();
        return other_response.equals(getResponse());
    }
}