package Response;

import java.io.Serializable;

public abstract class Response implements Serializable {
    Object response;

    public Response() {
    }

    public Object getResponse() {
        return response;
    }

    /**
     * Returns if two responses are equivalent
     *
     * @param r
     * @return
     */
    public boolean equals(Response r) {
        System.out.println("Base class call");
        return false;
    }

}
