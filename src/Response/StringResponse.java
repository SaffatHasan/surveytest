package Response;

/**
 * Represents all responses to a particular question (that are all strings)
 */
public class StringResponse extends Response {
    protected String response;

    public StringResponse(String resp) {
        this.response = resp;
    }

    public Object getResponse() {
        return response;
    }

    public boolean equals(Response r) {
        if (!(r instanceof StringResponse)) {
            return false;
        }

        String other_response = (String) r.getResponse();

        return other_response.equals(getResponse());
    }
}
