# Survey Test

## Author
Saffat Hasan

## Class
CS 350

## Description

This is a Survey and Test project. The functionality is to create, store,
tabulate, and take surveys and tests.

## Project Goals
This project was initially meant as practice for correct implementation of
design patterns in java. This project will now be used for the purpose of
understanding the use of DV8 and depends for dependency analysis and
recovering design patterns and architecture from existing projects without
use of documentation.

## Implementation Requirements
Below are some of the requirements for implementation.

### Types of Questions
- True/False
- Multiple Choice
- Matching
- Ranking
- Short Answer
- Essay

### Tests
Tests have the additional functionality of being gradeable. This implies that
questions can have a correct answer as well as being ungradeable for the case
of questions such as Essay Question.

## Design
The design for this project is documented in the pdf/png/svg files in the docs/
directory.
