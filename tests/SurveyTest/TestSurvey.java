package SurveyTest;

import IO.ConsoleInput;
import IO.ConsoleOutput;
import IO.Input;
import IO.Output;
import org.junit.Test;

public class TestSurvey {
    @Test
    public void test_survey_can_be_instantiated() {
        Output o = new ConsoleOutput();
        Input i = new ConsoleInput(o);
        Survey survey = new Survey(i, o, "fake name");
    }

    @Test
    public void test_test_can_be_instantiated() {
        Output o = new ConsoleOutput();
        Input i = new ConsoleInput(o);
        Survey test = new SurveyTest.Test(i, o, "fake name");
    }

}
